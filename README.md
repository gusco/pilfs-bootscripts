# PiLFS Bootscripts 20230824 v1.0
## Raspberry Pi specific bootscripts for LFS
### https://intestinate.com/pilfs

Contains the following install targets:
* networkfix          - Tells the kernel to keep 8MB of RAM free at all times for incoming network traffic
* swapfix             - Workaround for a problem with using swapfiles on boot
* fake-hwclock        - Adds a script to save/restore time between reboots
* rngd                - Stop/start script for the hardware random number generator daemon
* sshd-keygen         - Simple modification to the BLFS sshd script that will generate new keys if missing
* switch-cpu-governor - Sets the cpufreq governor to ondemand which allows for overclocking the Pi
* fanshim             - Provides automatic control of the Fan SHIM on Raspberry Pi 4
* rc.local            - An empty init script in which to add your own autostart applications
