EXTDIR=${DESTDIR}/etc
MAN8=${DESTDIR}/usr/share/man/man8
SBIN=${DESTDIR}/sbin
MODE=754
CONFMODE=644

all:
	@grep "^install" Makefile | cut -d ":" -f 1
	@echo "Select an appropriate install target from the above list"

install-everything: install-networkfix install-swapfix install-fake-hwclock install-sshd-keygen install-switch-cpu-governor install-rc.local

uninstall-everything: uninstall-networkfix uninstall-swapfix uninstall-fake-hwclock uninstall-sshd-keygen uninstall-switch-cpu-governor uninstall-rc.local

install-networkfix:
	echo "vm.min_free_kbytes=8192" >> ${EXTDIR}/sysctl.conf

uninstall-networkfix:
	sed -i '/vm.min_free_kbytes=8192/d' ${EXTDIR}/sysctl.conf

install-swapfix:
	mv ${EXTDIR}/rc.d/rcS.d/S20swap ${EXTDIR}/rc.d/rcS.d/S60swap
	echo "vm.swappiness=1" >> ${EXTDIR}/sysctl.conf

uninstall-swapfix:
	mv ${EXTDIR}/rc.d/rcS.d/S60swap ${EXTDIR}/rc.d/rcS.d/S20swap
	sed -i '/vm.swappiness=1/d' ${EXTDIR}/sysctl.conf

install-fake-hwclock:
	install -m ${MODE} sbin/fake-hwclock   ${SBIN}
	install -m ${MODE} sbin/fake-hwclock.8 ${MAN8}
	install -m ${MODE} init.d/fake-hwclock ${EXTDIR}/rc.d/init.d/
	ln -sf  ../init.d/fake-hwclock ${EXTDIR}/rc.d/rcS.d/S01fake-hwclock
	ln -sf  ../init.d/fake-hwclock ${EXTDIR}/rc.d/rc0.d/K01fake-hwclock
	ln -sf  ../init.d/fake-hwclock ${EXTDIR}/rc.d/rc1.d/K01fake-hwclock
	ln -sf  ../init.d/fake-hwclock ${EXTDIR}/rc.d/rc6.d/K01fake-hwclock

uninstall-fake-hwclock:
	rm ${SBIN}/fake-hwclock
	rm ${MAN8}/fake-hwclock.8
	rm ${EXTDIR}/rc.d/init.d/fake-hwclock
	rm ${EXTDIR}/rc.d/rcS.d/S01fake-hwclock
	rm ${EXTDIR}/rc.d/rc0.d/K01fake-hwclock
	rm ${EXTDIR}/rc.d/rc1.d/K01fake-hwclock
	rm ${EXTDIR}/rc.d/rc6.d/K01fake-hwclock

install-rngd:
	install -m ${MODE} init.d/rngd ${EXTDIR}/rc.d/init.d/
	ln -sf  ../init.d/rngd ${EXTDIR}/rc.d/rc0.d/K85rngd
	ln -sf  ../init.d/rngd ${EXTDIR}/rc.d/rc1.d/K85rngd
	ln -sf  ../init.d/rngd ${EXTDIR}/rc.d/rc2.d/K85rngd
	ln -sf  ../init.d/rngd ${EXTDIR}/rc.d/rc3.d/S15rngd
	ln -sf  ../init.d/rngd ${EXTDIR}/rc.d/rc4.d/S15rngd
	ln -sf  ../init.d/rngd ${EXTDIR}/rc.d/rc5.d/S15rngd
	ln -sf  ../init.d/rngd ${EXTDIR}/rc.d/rc6.d/K85rngd

uninstall-rngd:
	rm ${EXTDIR}/rc.d/init.d/rngd
	rm ${EXTDIR}/rc.d/rc0.d/K85rngd
	rm ${EXTDIR}/rc.d/rc1.d/K85rngd
	rm ${EXTDIR}/rc.d/rc2.d/K85rngd
	rm ${EXTDIR}/rc.d/rc3.d/S15rngd
	rm ${EXTDIR}/rc.d/rc4.d/S15rngd
	rm ${EXTDIR}/rc.d/rc5.d/S15rngd
	rm ${EXTDIR}/rc.d/rc6.d/K85rngd

install-sshd-keygen:
	install -m ${MODE} init.d/sshd ${EXTDIR}/rc.d/init.d/
	ln -sf  ../init.d/sshd ${EXTDIR}/rc.d/rc0.d/K30sshd
	ln -sf  ../init.d/sshd ${EXTDIR}/rc.d/rc1.d/K30sshd
	ln -sf  ../init.d/sshd ${EXTDIR}/rc.d/rc2.d/K30sshd
	ln -sf  ../init.d/sshd ${EXTDIR}/rc.d/rc3.d/S30sshd
	ln -sf  ../init.d/sshd ${EXTDIR}/rc.d/rc4.d/S30sshd
	ln -sf  ../init.d/sshd ${EXTDIR}/rc.d/rc5.d/S30sshd
	ln -sf  ../init.d/sshd ${EXTDIR}/rc.d/rc6.d/K30sshd

uninstall-sshd-keygen:
	rm ${EXTDIR}/rc.d/init.d/sshd
	rm ${EXTDIR}/rc.d/rc0.d/K30sshd
	rm ${EXTDIR}/rc.d/rc1.d/K30sshd
	rm ${EXTDIR}/rc.d/rc2.d/K30sshd
	rm ${EXTDIR}/rc.d/rc3.d/S30sshd
	rm ${EXTDIR}/rc.d/rc4.d/S30sshd
	rm ${EXTDIR}/rc.d/rc5.d/S30sshd
	rm ${EXTDIR}/rc.d/rc6.d/K30sshd

install-switch-cpu-governor:
	install -m ${MODE} init.d/switch_cpu_governor ${EXTDIR}/rc.d/init.d/
	ln -sf  ../init.d/switch_cpu_governor ${EXTDIR}/rc.d/rcS.d/S95switch_cpu_governor

uninstall-switch-cpu-governor:
	rm ${EXTDIR}/rc.d/init.d/switch_cpu_governor
	rm ${EXTDIR}/rc.d/rcS.d/S95switch_cpu_governor

install-fanshim:
	pip3 install fanshim psutil RPi.GPIO
	install -m ${MODE} sbin/fanshim-control.py ${SBIN}
	install -m ${MODE} init.d/fanshim ${EXTDIR}/rc.d/init.d/
	ln -sf  ../init.d/fanshim ${EXTDIR}/rc.d/rc0.d/K02fanshim
	ln -sf  ../init.d/fanshim ${EXTDIR}/rc.d/rc1.d/K02fanshim
	ln -sf  ../init.d/fanshim ${EXTDIR}/rc.d/rc2.d/K02fanshim
	ln -sf  ../init.d/fanshim ${EXTDIR}/rc.d/rc3.d/S98fanshim
	ln -sf  ../init.d/fanshim ${EXTDIR}/rc.d/rc4.d/S98fanshim
	ln -sf  ../init.d/fanshim ${EXTDIR}/rc.d/rc5.d/S98fanshim
	ln -sf  ../init.d/fanshim ${EXTDIR}/rc.d/rc6.d/K02fanshim

uninstall-fanshim:
	pip3 uninstall fanshim
	rm ${SBIN}/fanshim-control.py
	rm ${EXTDIR}/rc.d/init.d/fanshim
	rm ${EXTDIR}/rc.d/rc0.d/K02fanshim
	rm ${EXTDIR}/rc.d/rc1.d/K02fanshim
	rm ${EXTDIR}/rc.d/rc2.d/K02fanshim
	rm ${EXTDIR}/rc.d/rc3.d/S98fanshim
	rm ${EXTDIR}/rc.d/rc4.d/S98fanshim
	rm ${EXTDIR}/rc.d/rc5.d/S98fanshim
	rm ${EXTDIR}/rc.d/rc6.d/K02fanshim

install-rc.local:
	install -m ${MODE} init.d/rc.local ${EXTDIR}/rc.d/init.d/
	ln -sf  ../init.d/rc.local ${EXTDIR}/rc.d/rc0.d/K01rc.local
	ln -sf  ../init.d/rc.local ${EXTDIR}/rc.d/rc1.d/K01rc.local
	ln -sf  ../init.d/rc.local ${EXTDIR}/rc.d/rc2.d/K01rc.local
	ln -sf  ../init.d/rc.local ${EXTDIR}/rc.d/rc3.d/S99rc.local
	ln -sf  ../init.d/rc.local ${EXTDIR}/rc.d/rc4.d/S99rc.local
	ln -sf  ../init.d/rc.local ${EXTDIR}/rc.d/rc5.d/S99rc.local
	ln -sf  ../init.d/rc.local ${EXTDIR}/rc.d/rc6.d/K01rc.local

uninstall-rc.local:
	rm ${EXTDIR}/rc.d/init.d/rc.local
	rm ${EXTDIR}/rc.d/rc0.d/K01rc.local
	rm ${EXTDIR}/rc.d/rc1.d/K01rc.local
	rm ${EXTDIR}/rc.d/rc2.d/K01rc.local
	rm ${EXTDIR}/rc.d/rc3.d/S99rc.local
	rm ${EXTDIR}/rc.d/rc4.d/S99rc.local
	rm ${EXTDIR}/rc.d/rc5.d/S99rc.local
	rm ${EXTDIR}/rc.d/rc6.d/K01rc.local
